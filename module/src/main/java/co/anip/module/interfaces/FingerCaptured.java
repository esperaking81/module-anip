package co.anip.module.interfaces;

public interface FingerCaptured {
    public void onVerifyComplete(int res);
}
