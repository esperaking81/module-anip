package co.anip.module.utils;

import android.content.Context;
import android.widget.Toast;

public class Utils {
    private final Context ctx;

    public Utils(Context context) {
        this.ctx = context;
    }

    public void showMessage(String mes) {
        if (ctx != null) {
            Toast.makeText(ctx, mes, Toast.LENGTH_LONG).show();
        }
    }
}