package co.anip.module.utils;

public class Constants {
    public static final String DEVICE_MORE_CBM_ENABLED = "device_more_cbm_enabled";
    public static final String TPL = "template.iso-fmc-cs";
    public static final String WSQ = ".wsq";
    public static final String RAW = ".raw";
    // public static final String BIN = ".bin";
    public static final String JPEG = ".jpeg";
    public static final String SAVE_COMPLETE = "Capture terminée";
    public static final String SAVE_FAILED = "Échec de la capture";
    // public static final String CAPTURE_SUCCESS = "Successfully Captured.";
}
