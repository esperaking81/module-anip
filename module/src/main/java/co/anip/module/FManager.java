package co.anip.module;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.usb.UsbManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Keep;

import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.TemplateType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import co.anip.module.capture.AuthBfdCap;
import co.anip.module.capture.MorphoTabletFPSensorDevice;
import co.anip.module.interfaces.FingerCaptured;
import co.anip.module.utils.CommonFunctions;

import static co.anip.module.utils.Constants.DEVICE_MORE_CBM_ENABLED;
import static co.anip.module.utils.Constants.JPEG;
import static co.anip.module.utils.Constants.RAW;
import static co.anip.module.utils.Constants.WSQ;

@Keep
public class FManager implements AuthBfdCap {
    private static FManager _singleton = null;
    private String saveDir = BuildConfig.DEFAULT_CAP_DIR;
    public MorphoTabletFPSensorDevice fpSensorCap;
    private UsbManager usbmanager = null;
    private Activity activity;

    private boolean isCapturingTemplate = false;
    private boolean isCapturingImage = false;
    private boolean isCapturingBoth = false;
    private int iOutput = 0;
    private int tOutput = 0;

    private static Boolean allowDialogs = false;

    private FingerCaptured mListener;

    private FManager(Activity activity) {
        this.activity = activity;
        initFUtils(activity);
    }

    public static void setAllowDialog(Boolean value) {
        FManager.allowDialogs = value;
    }

    public static FManager getInstance(Activity activity) {
        if (_singleton == null) {
            _singleton = new FManager(activity);
        }
        return _singleton;
    }

    private Method MTAB_SetUsbPortSwitch = null;

    public static void setLogsPath(String path) {
        CommonFunctions.setLogsPath(path);
    }

    private void initFUtils(Activity ac) {
        int valueCBM = 1;
        try {
            valueCBM = Settings.System.getInt(ac.getContentResolver(), DEVICE_MORE_CBM_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            // CommonFunctions.saveLogs("INIT_F_UTILS", e.getMessage());
            Log.e("CommonFunctions", "Error first catch init: ", e);
            usbmanager = (UsbManager) ac.getSystemService(Context.USB_SERVICE);
            createMorphTabletUsbMethods();
            try {
                MTAB_SetUsbPortSwitch.invoke(usbmanager, 2, 1);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Log.e("CommonFunctions", "Error second catch init: ", e);
                // CommonFunctions.saveLogs("FManager", activity.getString(R.string.sensor_init_error));
            }
        }

        if (valueCBM == 1) {
            fpSensorCap = new MorphoTabletFPSensorDevice(this);
            fpSensorCap.open(ac);
        } else {
            Log.i("FM", activity.getString(R.string.sensor_init_error));
            // CommonFunctions.saveLogs("FManager", activity.getString(R.string.sensor_init_error));
            Log.e("FManager", activity.getString(R.string.sensor_init_error));
            Toast.makeText(this.activity, R.string.device_open_error, Toast.LENGTH_LONG).show();
        }
    }

    private void createMorphTabletUsbMethods() {
        Class<? extends UsbManager> usb = usbmanager.getClass();
        Class[] cArg1 = {int.class, int.class};

        try {
            MTAB_SetUsbPortSwitch = usb.getMethod("setUsbportSwitch", cArg1);
        } catch (NoSuchMethodException | IllegalArgumentException e) {
            Log.e("FManager", "CreateMorphoTabletUsbMethods resulted in: " + e.getMessage());
            // CommonFunctions.saveLogs("FManager", "CreateMorphoTabletUsbMethods resulted in: " + e.getMessage());
        }
    }

    public int verifyMatch(String template1, String template2) {
        initFUtils(activity);

        int returnValue;
        int res = fpSensorCap.verifyMatch(template1, template2);
        CommonFunctions.saveLogs("FManager", "verifyMatch result: " + res);
        Log.e("FManager", "verifyMatch result: " + res);

        if (res == ErrorCodes.MORPHO_OK) {
            returnValue = 1;
        } else {
            returnValue = 0;
        }
        return returnValue;
    }

    public void verify(int iType, String tplPath, FingerCaptured listener) {
        initFUtils(activity);

        byte[] tpl = CommonFunctions.pathToByte(tplPath);
        fpSensorCap.verify(iType, tpl, res -> {
            if (res == ErrorCodes.MORPHO_OK) {
                listener.onVerifyComplete(1);
            } else {
                listener.onVerifyComplete(0);
            }
        });
    }

    public void verify(String tplPath, FingerCaptured listener) {
        initFUtils(activity);

        byte[] tpl = CommonFunctions.pathToByte(tplPath);
        fpSensorCap.verify(15, tpl, res -> {
            if (res == ErrorCodes.MORPHO_OK) {
                listener.onVerifyComplete(1);
            } else {
                listener.onVerifyComplete(0);
            }
        });
    }

    public void captureBoth(String path, int iOutput, int tOutput, FingerCaptured listener) {
        this.isCapturingBoth = true;
        this.iOutput = iOutput;
        this.tOutput = tOutput;
        this.saveDir = path;
        this.mListener = listener;

        if (saveDir.endsWith("/")) {
            listener.onVerifyComplete(-1);

            initVars();
            Log.e("captureBoth", "Error: Invalid path !");
            CommonFunctions.saveLogs("captureBoth", "Error: Invalid path !");
            return;
        }

        try {
            initFUtils(activity);
            fpSensorCap.captureBoth(tOutput);
        } catch (Exception e) {
            listener.onVerifyComplete(0);

            initVars();
            Log.e("captureBoth", "Error: " + e.getMessage());
            CommonFunctions.saveLogs("captureBoth", "Error: " + e.getMessage());
        }
    }

    public void captureFinger(int output, String path, FingerCaptured listener) {
        this.isCapturingTemplate = true;
        this.tOutput = output;
        this.saveDir = path;
        this.mListener = listener;

        if (saveDir.endsWith("/")) {
            listener.onVerifyComplete(-1);

            initVars();
            Log.e("captureFinger", "Error: Invalid path !");
            CommonFunctions.saveLogs("captureFinger", "Error: Invalid path !");
            return;
        }

        try {
            initFUtils(activity);
            fpSensorCap.startCapture(output);
        } catch (Exception e) {
            listener.onVerifyComplete(0);

            initVars();
            Log.e("captureFinger", "Error: " + e.getMessage());
            CommonFunctions.saveLogs("captureFinger", "Error: " + e.getMessage());
        }
    }

    /**
     * int output
     * 0- WSQ
     * 1- RAW
     * 2- JPEG
     */
    public void captureImage(int output, String path, FingerCaptured listener) {
        this.isCapturingImage = true;
        this.iOutput = output;
        this.saveDir = path;
        this.mListener = listener;

        if (saveDir.endsWith("/")) {
            listener.onVerifyComplete(-1);

            initVars();
            CommonFunctions.saveLogs("captureImage", "Error: Invalid path !");
            Log.e("captureImage", "Error: Invalid path !");

            return;
        }

        try {
            initFUtils(activity);
            fpSensorCap.captureImage(output);
        } catch (Exception e) {
            listener.onVerifyComplete(0);

            initVars();
            Log.e("captureImage", "Error: " + e.getMessage());
            CommonFunctions.saveLogs("captureImage", "Error: " + e.getMessage());
        }
    }

    public void cancelAcquisition() {
        fpSensorCap.cancelLiveAcquisition();
    }

    public void releaseDevice() {
        fpSensorCap.release();
    }

    public void releaseResources() {
        cancelAcquisition();
        releaseDevice();
        initVars();
    }

    @Override
    public void updateImageView(ImageView imgPreview, Bitmap previewBitmap, String message, boolean flagComplete, int captureError) {

        if (isCapturingImage || isCapturingTemplate) {
            if (captureError == ErrorCodes.MORPHOERR_TIMEOUT) {
                Log.e("CAPTURE", "Capture Timeout ErrorCodes = " + captureError);
                CommonFunctions.saveLogs("CAPTURE", "Capture Timeout ErrorCodes = " + captureError);

                mListener.onVerifyComplete(0);
                releaseResources();
                return;
            } else if (captureError == ErrorCodes.MORPHOERR_CMDE_ABORTED) {
                Log.e("CAPTURE", "MORPHOERR_CMDE_ABORTED ErrorCodes = " + captureError);
                CommonFunctions.saveLogs("CAPTURE", "MORPHOERR_CMDE_ABORTED ErrorCodes = " + captureError);

                mListener.onVerifyComplete(0);
                releaseResources();
                return;
            }
        }

        if (isCapturingTemplate) {
            if (flagComplete && captureError == ErrorCodes.MORPHO_OK) {
                if (templateNotSaved()) return;

                mListener.onVerifyComplete(1);
                releaseResources();
                return;
            }
        }

        if (isCapturingImage) {
            if (flagComplete && captureError == ErrorCodes.MORPHO_OK) {
                if (imageNotSaved()) return;

                mListener.onVerifyComplete(1);
                releaseResources();
                return;
            }
        }

        if (isCapturingBoth) {
            if (flagComplete && captureError == ErrorCodes.MORPHO_OK) {
                if (templateNotSaved()) return;

                if (imageNotSaved()) return;

                mListener.onVerifyComplete(1);
                releaseResources();
            }
        }
    }

    private boolean templateNotSaved() {
        CommonFunctions.saveLogs("saving template", String.format("path %s processing...", saveDir));

        String fileExt = TemplateType.getValue(tOutput).getExtension();
        String filePath = CommonFunctions.saveFPToFile(fpSensorCap.templateBuffer, saveDir + fileExt);

        if (filePath == null) {
            mListener.onVerifyComplete(-1);
            initVars();
            return true;
        }
        CommonFunctions.saveLogs("template saved at: " + filePath, "processing...");

        return false;
    }

    private boolean imageNotSaved() {
        CommonFunctions.saveLogs("saving image", String.format("path %s processing...", saveDir));

        String fileExt = WSQ;
        byte[] imageData;

        if (iOutput == 0) {
            fileExt = WSQ;
        }

        if (iOutput == 1) {
            fileExt = RAW;
        }

        if (iOutput == 2) {
            fileExt = JPEG;
        }

        if (iOutput > 0 && iOutput <= 2) {
            imageData = fpSensorCap.rawImage;
        } else {
            imageData = fpSensorCap.compressedImage;
        }

        CommonFunctions.saveLogs("saving image", String.format("imageData is null ? %s", imageData == null));

        CommonFunctions.saveLogs("saving image iOutput: " + iOutput, "processing...");

        String filePath = CommonFunctions.saveFPToFile(imageData, saveDir + fileExt);

        if (filePath == null) {
            mListener.onVerifyComplete(-1);
            initVars();
            return true;
        }

        CommonFunctions.saveLogs("saved image at: " + filePath, "processing...");

        return false;
    }

    private void initVars() {
        isCapturingTemplate = false;
        isCapturingImage = false;
        isCapturingBoth = false;
        iOutput = 0;
        tOutput = 0;
        saveDir = BuildConfig.DEFAULT_CAP_DIR;
        mListener = null;
    }
}
