// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package co.anip.module.info.subtype;

public enum CaptureType
{
	Enroll, Verif
}
