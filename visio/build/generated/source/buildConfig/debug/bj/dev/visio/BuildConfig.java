/**
 * Automatically generated file. DO NOT MODIFY
 */
package bj.dev.visio;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "bj.dev.visio";
  public static final String BUILD_TYPE = "debug";
}
