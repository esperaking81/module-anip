package co.opensi.fingerprintmanager

import android.util.Log
import co.opensi.fingerprintmanager.wsq_utils.WSQUtils
import com.morpho.morphosmart.sdk.*
import java.io.DataInputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

object FingerPrintsDataManager {

    private val TAG = this.javaClass.canonicalName

    /* The type of the template type */
    private val TEMPLATE_TYPE = TemplateType.MORPHO_PK_ISO_FMC_CS
    /**
     * Morpho Device Capture Configuration
     */
    private const val ID_USER = "kkiapay"
    private val TEMPLATE_FVP_TYPE = TemplateFVPType.MORPHO_NO_PK_FVP


    /**
     * Write data in a file with FP format
     *
     * @param templateList containing data
     */
    fun exportFP(templateList: TemplateList) {
        val nbTemplate: Int = templateList.nbTemplate
        for (i in 0 until nbTemplate) {
            try {
                FileOutputStream("sdcard/TemplateFP_" + ID_USER + "_f" + (i + 1) + TEMPLATE_TYPE.extension).use { fos ->
                    val t = templateList.getTemplate(i)
                    val data: ByteArray = t.data
                    Log.d(TAG, "Writing data in file with FP format : " + data.contentToString())
                    fos.write(data)
                }
            } catch (e: IOException) {
                Log.e(TAG, "An error has occurred while manipulating files " + e.message)
            }
        }
    }

    fun getTemplateFromFile(dis: DataInputStream): Template? = try {
        val length = dis.available()
        val data = ByteArray(length)
        dis.readFully(data)

        val t = Template().apply {
            this.data = data
            this.templateType = TemplateType.MORPHO_NO_PK_FP
        }
        Log.d(TAG, "Template created from file.")
        t
    } catch (e: IOException){
        Log.e(TAG, "File reading error:" + e.message.toString())
        null
    }

    /**
     * Write data in a file with FVP format
     *
     * @param templateList containing data
     */
    fun exportFVP(templateList: TemplateList) {
        val nbTemplateFVP = templateList.nbFVPTemplate
        for (i in 0 until nbTemplateFVP) {
            try {
                FileOutputStream("sdcard/TemplateFVP_" + ID_USER + "_f" + (i + 1) + TEMPLATE_FVP_TYPE.extension).use { fos ->
                    val t = templateList.getFVPTemplate(i)
                    val data = t.data
                    Log.i(TAG, "Writing data in file with FVP format : " + Arrays.toString(data))
                    fos.write(data)
                }
            } catch (e: IOException) {
                Log.e(TAG, "An error has occurred while manipulating files " + e.message)
            }
        }
    }

    /**
     * Subtract the extension of a file and return it
     *
     * @param fileName to isolate extension
     * @return the extension
     */
    fun getFileExtension(fileName: String): String {
        var extension = ""
        val dotIndex = fileName.lastIndexOf('.')
        if (dotIndex >= 0) {
            extension = fileName.substring(dotIndex)
        }
        return extension
    }

    /**
     * Determines whether the Template is of type Template FP or Template FVP (depending on the
     * different Morpho devices)
     *
     * @param extension of the file
     * @return the type of Template
     */
    fun getTemplateTypeFromExtension(extension: String): ITemplateType {
        for (templateType in TemplateType.values()) {
            if (templateType.extension.equals(extension, ignoreCase = true)) {
                return templateType
            }
        }
        for (templateFVPType in TemplateFVPType.values()) {
            if (templateFVPType.extension.equals(extension, ignoreCase = true)) {
                return templateFVPType
            }
        }
        return TemplateType.MORPHO_NO_PK_FP
    }

    /**
     * Export fingerprints data to WSQ
     *
     * @param morphoImage to export
     */
    fun exportWSQCompressedImageWithHeader(morphoImage: MorphoImage) {
        try {
            FileOutputStream("sdcard/KkiapayUser_WSQ" + CompressionAlgorithm.MORPHO_COMPRESS_WSQ.extension).use { fileOutputStream ->
                val data = morphoImage.compressedImage
                val result: ByteArray = WSQUtils.setHeader(data)
                Log.i(TAG, "Writing data in file with WSQ format : " + result.contentToString())
                fileOutputStream.write(result)
            }
        } catch (e: IOException) {
            Log.e(TAG, "An error occurred while writing to sdcard: " + e.message)
        }
    }

    fun exportWSQCompressedImage(morphoImage: MorphoImage) {
        try {
            FileOutputStream("sdcard/KkiapayUser_WSQ" + CompressionAlgorithm.MORPHO_COMPRESS_WSQ.extension).use { fileOutputStream ->
                val data = morphoImage.compressedImage
                Log.i(TAG, "Writing data in file with WSQ format : " + data!!.contentToString())
                fileOutputStream.write(data)
            }
        } catch (e: IOException) {
            Log.e(TAG, "An error occurred while writing to sdcard: " + e.message)
        }
    }

    fun getWSQFileFromSDCARD(): ByteArray? {
        val dis = DataInputStream(FileInputStream(FingerPrintManager.WSQ_PATH))
        return try {
            val length = dis.available()
            val buffer = ByteArray(length)
            dis.readFully(buffer)
            buffer
        } catch (e: IOException){
            Log.e(TAG, "Error reading file")
            null
        }
    }
}