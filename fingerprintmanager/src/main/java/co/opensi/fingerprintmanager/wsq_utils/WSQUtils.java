package co.opensi.fingerprintmanager.wsq_utils;

import java.util.Arrays;

public class WSQUtils {
    public WSQUtils() {
    }

    public static byte[] setHeader(byte[] image) {
        byte[] imageNewHeader = hexStringToByteArray("FFA0FFA8007A4E4953545F434F4D20390A5049585F5749445448203235360A5049585F484549474854203430300A5049585F444550544820380A505049203530300A4C4F53535920310A434F4C4F52535041434520475241590A434F4D5052455353494F4E205753510A5753515F4249545241544520302E373530303030FFA8000846616D6F636FFFA4003A0907000932D325CD");
        byte[] imageTmp = Arrays.copyOfRange(image, 19, image.length);
        byte[] result = new byte[image.length - 19 + imageNewHeader.length];
        System.arraycopy(imageNewHeader, 0, result, 0, imageNewHeader.length);
        System.arraycopy(imageTmp, 0, result, imageNewHeader.length, imageTmp.length);
        return result;
    }

    static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for(int i = 0; i < len; i += 2) {
            data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
