package co.opensi.fingerprintmanager

// import com.gemalto.wsq.WSQDecoder
import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import co.opensi.fingerprintmanager.morpho_utils.DeviceDetectionMode
import co.opensi.fingerprintmanager.morpho_utils.MorphoUtils
import co.opensi.fingerprintmanager.morpho_utils.MyUtilityClass
import co.opensi.fingerprintmanager.morpho_utils.ProcessInfo
import com.morpho.android.usb.USBManager
import com.morpho.morphosmart.sdk.*
import java.io.DataInputStream
import java.io.FileInputStream
import java.io.IOException
import java.util.*
import kotlin.concurrent.thread

object FingerPrintManager {
    private val TAG = this.javaClass.canonicalName

    /* Data Management **/
    /**
     * Name of the Template file containing all the fingerprint data
     */
    private const val ID_USER = "test"
    private const val FILENAME = "TemplateFP_" + ID_USER + "_f1.iso-fmc-cs"
    private const val WSQ_FILE = "KkiapayUser_WSQ.wsq"

    /**
     * Path of the Template file containing all the fingerprint data
     */
    val FILEPATH = Environment.getExternalStorageDirectory().path + "/" + FILENAME
    val WSQ_PATH =
        Environment.getExternalStorageDirectory().path + "/" + WSQ_FILE

    private val dataManager = FingerPrintsDataManager
    private val processInfo: ProcessInfo = ProcessInfo.getInstance()
    private val mHandler: Handler = Handler()
    private val morphoDevice: MorphoDevice by lazyOf(MorphoDevice())
    private val deviceDetectionMode: DeviceDetectionMode = DeviceDetectionMode.SdkDetection

    private lateinit var sensorName: String

    /*
    * Init then connect to morphoDevice
    * */
    @JvmStatic
    fun connectToMorphoDevice(context: Context, packageName: String) {
        init(context, packageName)
        try {
            discoverDevices() && connection()
        } catch (e: Exception) {
            Log.d(TAG, e.message.toString())
        }
    }

    /*
    * Remove callbacks
    * */
    @JvmStatic
    fun dispose() {
        mHandler.removeCallbacksAndMessages(null)
    }

    @JvmStatic
    fun cancelConnection() {
        processInfo.morphoDevice.cancelLiveAcquisition()
    }

    /*
    * Close connection with morpho device
    * should be called when leaving app
    * */
    @JvmStatic
    fun closeConnection() {
        processInfo.morphoDevice.cancelLiveAcquisition()
        processInfo.morphoDevice.closeDevice()
    }

    /**
     * Capture of a fingerprint with Morpho device in a WSQ file format
     *
     * @param observer that will be notified in real time of the device acquisition
     */
    @JvmStatic
    fun morphoDeviceGetImage(
        observer: Observer,
        onCaptureCompleted: (result: Int, iError: Int) -> Unit
    ) {
        thread {
            val timeOut = processInfo.timeout
            var acquisitionThreshold = 0
            val compressionAlgorithm = CompressionAlgorithm.MORPHO_COMPRESS_WSQ
            val compressRate = 10
            val detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.value
            val latentDetection = LatentDetection.LATENT_DETECT_ENABLE
            val morphoImage = arrayOf(MorphoImage())
            var callbackCmd = processInfo.callbackCmd
            callbackCmd = callbackCmd and CallbackMask.MORPHO_CALLBACK_ENROLLMENT_CMD.value.inv()

            if (processInfo.isFingerprintQualityThreshold) acquisitionThreshold =
                processInfo.fingerprintQualityThresholdvalue

            val resultOfProcess = processInfo.morphoDevice.getImage(
                timeOut,
                acquisitionThreshold,
                compressionAlgorithm,
                compressRate,
                detectModeChoice,
                latentDetection,
                morphoImage[0],
                callbackCmd,
                observer
            )

            processInfo.isCommandBioStart = false
            MorphoUtils.storeFFDLogs(processInfo.morphoDevice)

            if (resultOfProcess == ErrorCodes.MORPHO_OK) {
                dataManager.exportWSQCompressedImage(morphoImage[0])
                //dataManager.exportWSQCompressedImageWithHeader(morphoImage[0])
            }

            val internalError = morphoDevice.internalError
            mHandler.post {
                synchronized(this) {
                    if (resultOfProcess != ErrorCodes.MORPHOERR_CMDE_ABORTED) {
                        onCaptureCompleted(resultOfProcess, internalError)
                    }
                }
            }
        }
    }

    @JvmStatic
    fun verify(observer: Observer, onMatch: (result: Int, iError: Int) -> Unit) {
        thread {
            val dis = DataInputStream(FileInputStream(FILEPATH))
            val templateList = TemplateList()
            val template = FingerPrintsDataManager.getTemplateFromFile(dis)
            templateList.putTemplate(template)

            val timeOut = 0
            val far = FalseAcceptanceRate.MORPHO_FAR_5
            val coderChoice = Coder.MORPHO_DEFAULT_CODER
            val detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.value
            val matchingStrategy = 0
            var callbackCmd = ProcessInfo.getInstance().callbackCmd

            callbackCmd =
                callbackCmd and CallbackMask.MORPHO_CALLBACK_ENROLLMENT_CMD.value.inv()

            val resultMatching = ResultMatching()

            var resultOfVerify =
                processInfo.morphoDevice.setStrategyAcquisitionMode(processInfo.strategyAcquisitionMode)

            if (resultOfVerify == 0) {
                resultOfVerify = processInfo.morphoDevice.verify(
                    timeOut, far, coderChoice,
                    detectModeChoice, matchingStrategy,
                    templateList, callbackCmd,
                    observer, resultMatching
                )
            }

            processInfo.isCommandBioStart = false

            val internalError = morphoDevice.internalError
            mHandler.post {
                synchronized(this) {
                    if (resultOfVerify != ErrorCodes.MORPHOERR_CMDE_ABORTED) {
                        onMatch(resultOfVerify, internalError)
                    }
                }
            }
        }
    }

//    fun displayFingerprintInImageView(imageView: ImageView) {
//        val dis = DataInputStream(FileInputStream(WSQ_PATH))
//        try {
//            val bitmap = WSQDecoder.decode(dis)
//            if (bitmap != null) {
//                imageView.setImageBitmap(bitmap)
//            } else {
//                Toast.makeText(imageView.context, "Image could not be decoded !", Toast.LENGTH_LONG)
//                    .show()
//            }
//        } catch (e: IOException) {
//            Log.e(TAG, "Error reading file !")
//        }
//    }

    /**
     * Start the capture of a fingerprint with Morpho device
     *
     * @param observer that will be notified in real time of the device acquisition
     */
    @JvmStatic
    @JvmOverloads
    fun morphoDeviceCapture(
        observer: Observer,
        cb: FingerprintManagerCallback,
        acquisitionStrategy: AcquisitionStrategy = AcquisitionStrategy.EXPERT_MODE
    ) {
        thread {
            val morphoImage = arrayOf(MorphoImage())

            val nbFinger = 1
            val maxSizeTemplate = 255
            val templateType = TemplateType.MORPHO_PK_ISO_FMC_CS
            val templateFvpType = TemplateFVPType.MORPHO_NO_PK_FVP
            val enrollType = EnrollmentType.ONE_ACQUISITIONS
            val timeout = processInfo.timeout
            val templateList = TemplateList()
            val latentDetection = LatentDetection.LATENT_DETECT_ENABLE
            val coderChoice: Coder = processInfo.coder
            var detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.value

            if (processInfo.isForceFingerPlacementOnTop) detectModeChoice =
                detectModeChoice or DetectionMode.MORPHO_FORCE_FINGER_ON_TOP_DETECT_MODE.value
            if (processInfo.isWakeUpWithLedOff) detectModeChoice =
                detectModeChoice or MorphoWakeUpMode.MORPHO_WAKEUP_LED_OFF.code

            val acquisitionThreshold =
                if (processInfo.isFingerprintQualityThreshold) processInfo.fingerprintQualityThresholdvalue else 0
            val advancedSecurityLevelsRequired =
                if (processInfo.isAdvancedSecLevCompReq) 1 else 0xFF

            val callbackCmd = ProcessInfo.getInstance().callbackCmd

            var resultAcquisition = handleAcquisitionStrategy(acquisitionStrategy)

            if (resultAcquisition == ErrorCodes.MORPHO_OK) {
                resultAcquisition = processInfo.morphoDevice.capture(
                    timeout, acquisitionThreshold,
                    advancedSecurityLevelsRequired, nbFinger,
                    templateType, templateFvpType,
                    maxSizeTemplate, enrollType,
                    latentDetection, coderChoice,
                    detectModeChoice, CompressionAlgorithm.MORPHO_NO_COMPRESS,
                    0, templateList,
                    callbackCmd, observer
                )
                processInfo.isCommandBioStart = false
                MorphoUtils.storeFFDLogs(processInfo.morphoDevice)
            }
            if (resultAcquisition == ErrorCodes.MORPHO_OK) {
                dataManager.exportFVP(templateList)
                dataManager.exportFP(templateList)
            }

            val internalError: Int = morphoDevice.internalError
            val retValue = resultAcquisition

            mHandler.post {
                synchronized(this) {
                    if (retValue != ErrorCodes.MORPHOERR_CMDE_ABORTED) cb.onComplete(
                        retValue,
                        internalError
                    )
                }
            }
        }
    }

    fun morphoDeviceVerify(
        observer: Observer,
        onVerifyCompleted: (result: Int, iError: Int) -> Unit
    ) {
        val dis = DataInputStream(FileInputStream(FILEPATH))
        try {
            val length = dis.available()
            val buffer = ByteArray(length)
            dis.readFully(buffer)

            thread {
                val template = Template()
                val templateFVP = TemplateFVP()
                val templateList = TemplateList()

                val iTemplateType: ITemplateType =
                    dataManager.getTemplateTypeFromExtension(dataManager.getFileExtension(FILENAME))

                if (iTemplateType is TemplateFVPType) {
                    templateFVP.data = buffer
                    templateFVP.templateFVPType = iTemplateType
                    templateList.putFVPTemplate(templateFVP)
                } else {
                    template.data = buffer
                    template.templateType = iTemplateType as TemplateType
                    templateList.putTemplate(template)
                }

                val timeOut = 0
                val far = FalseAcceptanceRate.MORPHO_FAR_5
                val coderChoice = Coder.MORPHO_DEFAULT_CODER
                val detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE.value
                val matchingStrategy = 0
                var callbackCmd = ProcessInfo.getInstance().callbackCmd

                callbackCmd =
                    callbackCmd and CallbackMask.MORPHO_CALLBACK_ENROLLMENT_CMD.value.inv()

                val resultMatching = ResultMatching()

                var resultOfVerify =
                    processInfo.morphoDevice.setStrategyAcquisitionMode(processInfo.strategyAcquisitionMode)

                if (resultOfVerify == 0) {
                    resultOfVerify = processInfo.morphoDevice.verify(
                        timeOut, far, coderChoice,
                        detectModeChoice, matchingStrategy,
                        templateList, callbackCmd,
                        observer, resultMatching
                    )
                }
                ProcessInfo.getInstance().isCommandBioStart = false
                MorphoUtils.storeFFDLogs(processInfo.morphoDevice)

                val result = resultOfVerify
                val internalError = processInfo.morphoDevice.internalError
                mHandler.post {
                    synchronized(this) {
                        if (result != ErrorCodes.MORPHOERR_CMDE_ABORTED) {
                            onVerifyCompleted(result, internalError)
                        }
                    }
                }
            }

        } catch (e: IOException) {
            Log.e(TAG, "Exception encountered with file verification : " + e.message)
        }
    }

    /* Callback for fingerprint device updates */
    /**
     * handleQuality()
     * Update UI Thread with quality returned by Morpho device
     *
     * @param i used to set new progress
     */
    /**
     * handleImage()
     * Update UI Thread with image return by Morpho device
     *
     * @param i to display
     */
    /**
     * handleCommand()
     * Update UI Thread with message return by Morpho device
     *
     * @param i corresponding to a String to display
     */
    @JvmStatic
    fun onCaptureOrVerificationUpdate(
        arg: Any?,
        cb: FingerprintManagerCallback,
    ) {
        val message = arg as CallbackMessage

        mHandler.post {
            synchronized(this) {
                when (message.messageType) {
                    // Message is a command.
                    1 -> cb.handleCommand(message.message as Int)
                    // Message is a low resolution image
                    2 -> cb.handleImage(message.message as ByteArray)
                    // Message is the coded image quality.
                    3 -> cb.handleQuality(message.message as Int)
                    else -> Log.e(TAG, "Unknown message received from Morpho device")
                }
            }
        }
    }

    /*
    * Private functions
    */
    private fun handleAcquisitionStrategy(strategyAcquisitionMode: AcquisitionStrategy): Int {
        return when (strategyAcquisitionMode) {
            AcquisitionStrategy.EXPERT_MODE -> processInfo.morphoDevice.setStrategyAcquisitionMode(
                StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE
            )
            AcquisitionStrategy.ACCURATE_MODE -> processInfo.morphoDevice.setStrategyAcquisitionMode(
                StrategyAcquisitionMode.MORPHO_ACQ_UNIVERSAL_ACCURATE_MODE
            )
            AcquisitionStrategy.MULTI_MODAL_MODE -> processInfo.morphoDevice.setStrategyAcquisitionMode(
                StrategyAcquisitionMode.MORPHO_ACQ_FULL_MULTIMODAL_MODE
            )
            AcquisitionStrategy.FAST_MODE -> processInfo.morphoDevice.setStrategyAcquisitionMode(
                StrategyAcquisitionMode.MORPHO_ACQ_UNIVERSAL_FAST_MODE
            )
            AcquisitionStrategy.ANTI_SPOOFING_MODE -> processInfo.morphoDevice.setStrategyAcquisitionMode(
                StrategyAcquisitionMode.MORPHO_ACQ_ANTI_SPOOFING_MODE
            )
        }
    }

    // Initialize morpho device
    private fun init(context: Context, packageName: String) {
        USBManager.getInstance().initialize(context, "${packageName}.USB_ACTION")
        processInfo.morphoDevice = morphoDevice
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())
    }

    // Discover devices
    private fun discoverDevices(): Boolean {
        val myUtilityClass = MyUtilityClass()
        val nbUsbDevice = myUtilityClass.nbUsbDevice
        val resultCode = processInfo.morphoDevice.initUsbDevicesNameEnum(nbUsbDevice)
        return if (resultCode == ErrorCodes.MORPHO_OK) {
            Log.i(TAG, "Morpho OK")
            if (nbUsbDevice > 0) {
                Log.d(TAG, "Some Devices Found")
                sensorName = processInfo.morphoDevice.getUsbDeviceName(0)
                true
            } else {
                Log.e(TAG, "No Device Found")
                throw Exception("NO DEVICE FOUND")
            }
        } else {
            Log.e(TAG, ErrorCodes.getError(resultCode, processInfo.morphoDevice.internalError))
            throw Exception(ErrorCodes.getError(resultCode, processInfo.morphoDevice.internalError))
        }
    }

    // Effective connection to morpho device
    @SuppressLint("UseValueOf")
    private fun connection(): Boolean {
        var ret = ErrorCodes.MORPHO_OK
        if (deviceDetectionMode == DeviceDetectionMode.SdkDetection) {
            Log.i(TAG, "Device sensor name: $sensorName")
            ret = processInfo.morphoDevice.openUsbDevice(sensorName, 0)
        }
        return if (ret != ErrorCodes.MORPHO_OK) {
            Log.e(TAG, ErrorCodes.getError(ret, processInfo.morphoDevice.internalError))
            throw Exception(ErrorCodes.getError(ret, processInfo.morphoDevice.internalError))
        } else {
            Log.i(TAG, "Device connected !")
            true
        }
    }
}