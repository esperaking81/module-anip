package co.opensi.fingerprintmanager

object ErrorCodes {
    //
// Source code recreated from a .class file by IntelliJ IDEA
//
    const val MORPHO_OK = 0
    const val MORPHOERR_INTERNAL = -1
    const val MORPHOERR_PROTOCOLE = -2
    const val MORPHOERR_CONNECT = -3
    const val MORPHOERR_CLOSE_COM = -4
    const val MORPHOERR_BADPARAMETER = -5
    const val MORPHOERR_MEMORY_PC = -6
    const val MORPHOERR_MEMORY_DEVICE = -7
    const val MORPHOERR_NO_HIT = -8
    const val MORPHOERR_STATUS = -9
    const val MORPHOERR_DB_FULL = -10
    const val MORPHOERR_DB_EMPTY = -11
    const val MORPHOERR_ALREADY_ENROLLED = -12
    const val MORPHOERR_BASE_NOT_FOUND = -13
    const val MORPHOERR_BASE_ALREADY_EXISTS = -14
    const val MORPHOERR_NO_ASSOCIATED_DB = -15
    const val MORPHOERR_NO_ASSOCIATED_DEVICE = -16
    const val MORPHOERR_INVALID_TEMPLATE = -17
    const val MORPHOERR_NOT_IMPLEMENTED = -18
    const val MORPHOERR_TIMEOUT = -19
    const val MORPHOERR_NO_REGISTERED_TEMPLATE = -20
    const val MORPHOERR_FIELD_NOT_FOUND = -21
    const val MORPHOERR_CORRUPTED_CLASS = -22
    const val MORPHOERR_TO_MANY_TEMPLATE = -23
    const val MORPHOERR_TO_MANY_FIELD = -24
    const val MORPHOERR_MIXED_TEMPLATE = -25
    const val MORPHOERR_CMDE_ABORTED = -26
    const val MORPHOERR_INVALID_PK_FORMAT = -27
    const val MORPHOERR_SAME_FINGER = -28
    const val MORPHOERR_OUT_OF_FIELD = -29
    const val MORPHOERR_INVALID_USER_ID = -30
    const val MORPHOERR_INVALID_USER_DATA = -31
    const val MORPHOERR_FIELD_INVALID = -32
    const val MORPHOERR_USER_NOT_FOUND = -33
    const val MORPHOERR_COM_NOT_OPEN = -34
    const val MORPHOERR_ELT_ALREADY_PRESENT = -35
    const val MORPHOERR_NOCALLTO_DBQUERRYFIRST = -36
    const val MORPHOERR_USER = -37
    const val MORPHOERR_BAD_COMPRESSION = -38
    const val MORPHOERR_SECU = -39
    const val MORPHOERR_CERTIF_UNKNOW = -40
    const val MORPHOERR_INVALID_CLASS = -41
    const val MORPHOERR_USB_DEVICE_NAME_UNKNOWN = -42
    const val MORPHOERR_CERTIF_INVALID = -43
    const val MORPHOERR_SIGNER_ID = -44
    const val MORPHOERR_SIGNER_ID_INVALID = -45
    const val MORPHOERR_FFD = -46
    const val MORPHOERR_MOIST_FINGER = -47
    const val MORPHOERR_NO_SERVER = -48
    const val MORPHOERR_OTP_NOT_INITIALIZED = -49
    const val MORPHOERR_OTP_PIN_NEEDED = -50
    const val MORPHOERR_OTP_REENROLL_NOT_ALLOWED = -51
    const val MORPHOERR_OTP_ENROLL_FAILED = -52
    const val MORPHOERR_OTP_IDENT_FAILED = -53
    const val MORPHOERR_NO_MORE_OTP = -54
    const val MORPHOERR_OTP_NO_HIT = -55
    const val MORPHOERR_OTP_ENROLL_NEEDED = -56
    const val MORPHOERR_DEVICE_LOCKED = -57
    const val MORPHOERR_DEVICE_NOT_LOCK = -58
    const val MORPHOERR_OTP_LOCK_GEN_OTP = -59
    const val MORPHOERR_OTP_LOCK_SET_PARAM = -60
    const val MORPHOERR_OTP_LOCK_ENROLL = -61
    const val MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH = -62
    const val MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN = -63
    const val MORPHOERR_LICENSE_MISSING = -64
    const val MORPHOERR_ADVANCED_SECURITY_LEVEL_MISMATCH = -65
    const val MORPHOERR_BAD_FINAL_FINGER_PRINT_QUALITY = -66
    const val MORPHOERR_FFD_FINGER_MISPLACED = -68
    const val MORPHOERR_KEY_NOT_FOUND = -69
    const val MORPHOWARNING_WSQ_COMPRESSION_RATIO = -70
    const val MORPHOERR_ADVANCED_SECURITY_LEVEL_NOT_AVAILABLE = -71
    const val MORPHOERR_UNAVAILABLE = -72
    const val MORPHOERR_MOVED_FINGER = -73
    const val MORPHOERR_SATURATED_FINGER = -74
    const val MORPHOERR_INVALID_FINGER = -75
    const val MORPHOERR_SVC_LOST_DEVICE = -110
    const val MORPHOERR_CANT_GRAN_PERMISSION_USB = -99
    const val CLASS_NOT_INSTANTIATED = -98
    const val MORPHOERR_USB_PERMISSION_DENIED = -97
    const val MORPHOERR_RESUME_CONNEXION_ALREADY_STARTED = -96
    const val MORPHOERR_RESUME_CONNEXION = -95
    const val MORPHOERR_INTEGER_INITIALIZATION = -94
    const val MORPHOERR_LONG_INITIALIZATION = -93
    fun getError(codeError: Int, internalError: Int): String {
        return when (codeError) {
            -110 -> "A connected device to the Android service has been lost, this can occur when the Android OS goes to sleep or device detached."
            -109, -108, -107, -106, -105, -104, -103, -102, -101, -100, -98, -92, -91, -90, -89, -88, -87, -86, -85, -84, -83, -82, -81, -80, -79, -78, -77, -76, -72, -71, -70, -69, -68, -67, -66, -65 -> String.format(
                "Unknown error %d, Internal Error = %d",
                codeError,
                internalError
            )
            -99 -> "Could not grant permissions to USB"
            -97 -> "USB permission denied."
            -96 -> "Resume Connexion Already Started."
            -95 -> "Cannot connect biometrics device. USB Permission denied."
            -94 -> "Integer initialization error, you must initialize the Integer object with new Integer(0)."
            -93 -> "Long initialization error, you must initialize the Long object with new Long(0)."
            -75 -> "The finger is invalid."
            -74 -> "The finger can be too shiny or too much external light."
            -73 -> "The finger moved during acquisition or removed earlier."
            -64 -> "A required license is missing."
            -63 -> "Misplaced or withdrawn finger has been detected during acquisition (MorphoSmart FINGER VP only)."
            -62 -> "Security level mismatch: attempt to match fingerprint template in high security level (MorphoSmart FINGER VP only)."
            -61 -> "ILV_OTP_ENROLL_USER Locked."
            -60 -> "ILV_OTP_SET_PARAMETERS  Locked."
            -59 -> "ILV_OTP_GENERATE Locked."
            -58 -> "The device is not locked."
            -57 -> "The device is locked."
            -56 -> "Enrollment needed before generating OTP."
            -55 -> "Authentication or Identification failed."
            -54 -> "No more OTP available (sequence number = 0)."
            -53 -> "Identification failed."
            -52 -> "Enrollment failed."
            -51 -> "User is not allowed to be reenrolled."
            -50 -> "Code pin is needed : it is the first enrollment."
            -49 -> "No parameter has been initialized."
            -48 -> "The Morpho MorphoSmart Service Provider Usb Server is stopped or not installed."
            -47 -> "The finger can be too moist or the scanner is wet."
            -46 -> "False Finger Detected."
            -45 -> "The X984 certificate identity size is different to 20 octets (SHA_1 size)."
            -44 -> "The certificate identity is not the same than the X984 certificate identity."
            -43 -> "The certificate is not valid."
            -42 -> "The specified Usb device is not plugged."
            -41 -> "The class has been destroyed."
            -40 -> "The MSO has not the certificate necessary to verify the signature."
            -39 -> "Security error."
            -38 -> "The Compression is not valid."
            -37 -> "The communication callback functions returns error between -10000 and -10499."
            -36 -> "You have to call C_MORPHO_Database::DbQueryFirst to initialize the querry."
            -35 -> "This element is already present in the list."
            -34 -> "Serial COM has not been opened."
            -33 -> "User is not found."
            -32 -> "Additional field name length is more than MORPHO_FIELD_NAME_LEN."
            -31 -> "The user data are not valid."
            -30 -> "UserID is not valid."
            -29 -> "The number of the additional field is more than 128."
            -28 -> "User gave twice the same finger."
            -27 -> "Invalid PK format."
            -26 -> "Command has been aborted."
            -25 -> "Templates with differents formats are mixed."
            -24 -> "There are too many fields."
            -23 -> "There are too many templates."
            -22 -> "Class has been corrupted."
            -21 -> "Field does not exist."
            -20 -> "No templates have been registered."
            -19 -> "No response after defined time."
            -18 -> "Command not yet implemented in this release."
            -17 -> "The template is not valid."
            -16 -> "Database object has been instanciated without C_MORPHO_Device::GetDatabase."
            -15 -> "User object has been instanciated without C_MORPHO_Database::GetUser."
            -14 -> "The specified base already exist."
            -13 -> "The specified base does not exist."
            -12 -> "User has already been enrolled."
            -11 -> "The database is empty."
            -10 -> "The database is full."
            -9 -> "MSO returned an unknown status error."
            -8 -> "Authentication or Identification failed."
            -7 -> "Not enough memory for the creation of a database in the MSO."
            -6 -> "Not enough memory (in the PC)."
            -5 -> "Invalid parameter."
            -4 -> "Error while closing communication port."
            -3 -> "Can not connect biometrics device."
            -2 -> "Communication protocole error."
            -1 -> "Biometrics device performed an internal error."
            0 -> "No error."
            1 -> "Operation Failed"
            2 -> "Operation completed successfully"
            else -> String.format(
                "Unknown error %d, Internal Error = %d",
                codeError,
                internalError
            )
        }
    }
}