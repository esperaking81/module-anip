package co.opensi.fingerprintmanager

enum class AcquisitionStrategy(
    private val code: Int,
    private val label: String
) {
    EXPERT_MODE(0, "Expert(Default)"),
    FAST_MODE(1, "Fast(Standard)"),
    ACCURATE_MODE(2, "Slow(Accurate)"),
    MULTI_MODAL_MODE(3, "Full MultiModal"),
    ANTI_SPOOFING_MODE(4, "Anti spoofing");

    fun getValue(id: Int): AcquisitionStrategy? {
        val acquisitionStrategies: Array<AcquisitionStrategy> = values()

        for (i in 0..acquisitionStrategies.size){
            if (acquisitionStrategies[i].code == id){
                return acquisitionStrategies[i]
            }
        }
        return null
    }
}