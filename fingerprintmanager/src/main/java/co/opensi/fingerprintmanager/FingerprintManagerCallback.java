package co.opensi.fingerprintmanager;

public interface FingerprintManagerCallback {
    void onComplete(int result, int iError);

    void handleCommand(int res);

    void handleImage(byte[] i);

    void handleQuality(int i);
}
