package co.opensi.fingerprintmanager

import android.content.Context
import android.view.View
import java.util.*

object ErrorManager {
    fun convertError(iCodeError: Int, iInternalError: Int, context: Context): String{
        context.resources.run {
            return when (iCodeError) {
                ErrorCodes.MORPHO_OK -> getString(R.string.MORPHO_OK)
                ErrorCodes.MORPHOERR_INTERNAL -> getString(R.string.MORPHOERR_INTERNAL)
                ErrorCodes.MORPHOERR_PROTOCOLE -> getString(R.string.MORPHOERR_PROTOCOLE)
                ErrorCodes.MORPHOERR_CONNECT -> getString(R.string.MORPHOERR_CONNECT)
                ErrorCodes.MORPHOERR_CLOSE_COM -> getString(R.string.MORPHOERR_CLOSE_COM)
                ErrorCodes.MORPHOERR_BADPARAMETER -> getString(R.string.MORPHOERR_BADPARAMETER)
                ErrorCodes.MORPHOERR_MEMORY_PC -> getString(R.string.MORPHOERR_MEMORY_PC)
                ErrorCodes.MORPHOERR_MEMORY_DEVICE -> getString(R.string.MORPHOERR_MEMORY_DEVICE)
                ErrorCodes.MORPHOERR_NO_HIT -> getString(R.string.MORPHOERR_NO_HIT)
                ErrorCodes.MORPHOERR_STATUS -> getString(R.string.MORPHOERR_STATUS)
                ErrorCodes.MORPHOERR_DB_FULL -> getString(R.string.MORPHOERR_DB_FULL)
                ErrorCodes.MORPHOERR_DB_EMPTY -> getString(R.string.MORPHOERR_DB_EMPTY)
                ErrorCodes.MORPHOERR_ALREADY_ENROLLED -> getString(R.string.MORPHOERR_ALREADY_ENROLLED)
                ErrorCodes.MORPHOERR_BASE_NOT_FOUND -> getString(R.string.MORPHOERR_BASE_NOT_FOUND)
                ErrorCodes.MORPHOERR_BASE_ALREADY_EXISTS -> getString(
                    R.string.MORPHOERR_BASE_ALREADY_EXISTS
                )
                ErrorCodes.MORPHOERR_NO_ASSOCIATED_DB -> getString(R.string.MORPHOERR_NO_ASSOCIATED_DB)
                ErrorCodes.MORPHOERR_NO_ASSOCIATED_DEVICE -> getString(
                    R.string.MORPHOERR_NO_ASSOCIATED_DEVICE
                )
                ErrorCodes.MORPHOERR_INVALID_TEMPLATE -> getString(R.string.MORPHOERR_INVALID_TEMPLATE)
                ErrorCodes.MORPHOERR_NOT_IMPLEMENTED -> getString(R.string.MORPHOERR_NOT_IMPLEMENTED)
                ErrorCodes.MORPHOERR_TIMEOUT -> getString(R.string.MORPHOERR_TIMEOUT)
                ErrorCodes.MORPHOERR_NO_REGISTERED_TEMPLATE -> getString(
                    R.string.MORPHOERR_NO_REGISTERED_TEMPLATE
                )
                ErrorCodes.MORPHOERR_FIELD_NOT_FOUND -> getString(R.string.MORPHOERR_FIELD_NOT_FOUND)
                ErrorCodes.MORPHOERR_CORRUPTED_CLASS -> getString(R.string.MORPHOERR_CORRUPTED_CLASS)
                ErrorCodes.MORPHOERR_TO_MANY_TEMPLATE -> getString(R.string.MORPHOERR_TO_MANY_TEMPLATE)
                ErrorCodes.MORPHOERR_TO_MANY_FIELD -> getString(R.string.MORPHOERR_TO_MANY_FIELD)
                ErrorCodes.MORPHOERR_MIXED_TEMPLATE -> getString(R.string.MORPHOERR_MIXED_TEMPLATE)
                ErrorCodes.MORPHOERR_CMDE_ABORTED -> getString(R.string.MORPHOERR_CMDE_ABORTED)
                ErrorCodes.MORPHOERR_INVALID_PK_FORMAT -> getString(R.string.MORPHOERR_INVALID_PK_FORMAT)
                ErrorCodes.MORPHOERR_SAME_FINGER -> getString(R.string.MORPHOERR_SAME_FINGER)
                ErrorCodes.MORPHOERR_OUT_OF_FIELD -> getString(R.string.MORPHOERR_OUT_OF_FIELD)
                ErrorCodes.MORPHOERR_INVALID_USER_ID -> getString(R.string.MORPHOERR_INVALID_USER_ID)
                ErrorCodes.MORPHOERR_INVALID_USER_DATA -> getString(R.string.MORPHOERR_INVALID_USER_DATA)
                ErrorCodes.MORPHOERR_FIELD_INVALID -> getString(R.string.MORPHOERR_FIELD_INVALID)
                ErrorCodes.MORPHOERR_USER_NOT_FOUND -> getString(R.string.MORPHOERR_USER_NOT_FOUND)
                ErrorCodes.MORPHOERR_COM_NOT_OPEN -> getString(R.string.MORPHOERR_COM_NOT_OPEN)
                ErrorCodes.MORPHOERR_ELT_ALREADY_PRESENT -> getString(
                    R.string.MORPHOERR_ELT_ALREADY_PRESENT
                )
                ErrorCodes.MORPHOERR_NOCALLTO_DBQUERRYFIRST -> getString(
                    R.string.MORPHOERR_NOCALLTO_DBQUERRYFIRST
                )
                ErrorCodes.MORPHOERR_USER -> getString(R.string.MORPHOERR_USER)
                ErrorCodes.MORPHOERR_BAD_COMPRESSION -> getString(R.string.MORPHOERR_BAD_COMPRESSION)
                ErrorCodes.MORPHOERR_SECU -> getString(R.string.MORPHOERR_SECU)
                ErrorCodes.MORPHOERR_CERTIF_UNKNOW -> getString(R.string.MORPHOERR_CERTIF_UNKNOW)
                ErrorCodes.MORPHOERR_INVALID_CLASS -> getString(R.string.MORPHOERR_INVALID_CLASS)
                ErrorCodes.MORPHOERR_USB_DEVICE_NAME_UNKNOWN -> getString(
                    R.string.MORPHOERR_USB_DEVICE_NAME_UNKNOWN
                )
                ErrorCodes.MORPHOERR_CERTIF_INVALID -> getString(R.string.MORPHOERR_CERTIF_INVALID)
                ErrorCodes.MORPHOERR_SIGNER_ID -> getString(R.string.MORPHOERR_SIGNER_ID)
                ErrorCodes.MORPHOERR_SIGNER_ID_INVALID -> getString(R.string.MORPHOERR_SIGNER_ID_INVALID)
                ErrorCodes.MORPHOERR_FFD -> getString(R.string.MORPHOERR_FFD)
                ErrorCodes.MORPHOERR_MOIST_FINGER -> getString(R.string.MORPHOERR_MOIST_FINGER)
                ErrorCodes.MORPHOERR_NO_SERVER -> getString(R.string.MORPHOERR_NO_SERVER)
                ErrorCodes.MORPHOERR_OTP_NOT_INITIALIZED -> getString(
                    R.string.MORPHOERR_OTP_NOT_INITIALIZED
                )
                ErrorCodes.MORPHOERR_OTP_PIN_NEEDED -> getString(R.string.MORPHOERR_OTP_PIN_NEEDED)
                ErrorCodes.MORPHOERR_OTP_REENROLL_NOT_ALLOWED -> getString(
                    R.string.MORPHOERR_OTP_REENROLL_NOT_ALLOWED
                )
                ErrorCodes.MORPHOERR_OTP_ENROLL_FAILED -> getString(R.string.MORPHOERR_OTP_ENROLL_FAILED)
                ErrorCodes.MORPHOERR_OTP_IDENT_FAILED -> getString(R.string.MORPHOERR_OTP_IDENT_FAILED)
                ErrorCodes.MORPHOERR_NO_MORE_OTP -> getString(R.string.MORPHOERR_NO_MORE_OTP)
                ErrorCodes.MORPHOERR_OTP_NO_HIT -> getString(R.string.MORPHOERR_OTP_NO_HIT)
                ErrorCodes.MORPHOERR_OTP_ENROLL_NEEDED -> getString(R.string.MORPHOERR_OTP_ENROLL_NEEDED)
                ErrorCodes.MORPHOERR_DEVICE_LOCKED -> getString(R.string.MORPHOERR_DEVICE_LOCKED)
                ErrorCodes.MORPHOERR_DEVICE_NOT_LOCK -> getString(R.string.MORPHOERR_DEVICE_NOT_LOCK)
                ErrorCodes.MORPHOERR_OTP_LOCK_GEN_OTP -> getString(R.string.MORPHOERR_OTP_LOCK_GEN_OTP)
                ErrorCodes.MORPHOERR_OTP_LOCK_SET_PARAM -> getString(R.string.MORPHOERR_OTP_LOCK_SET_PARAM)
                ErrorCodes.MORPHOERR_OTP_LOCK_ENROLL -> getString(R.string.MORPHOERR_OTP_LOCK_ENROLL)
                ErrorCodes.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH -> getString(
                    R.string.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH
                )
                ErrorCodes.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN -> getString(
                    R.string.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN
                )
                ErrorCodes.MORPHOERR_LICENSE_MISSING -> getString(R.string.MORPHOERR_LICENSE_MISSING)
                ErrorCodes.MORPHOERR_CANT_GRAN_PERMISSION_USB -> getString(
                    R.string.MORPHOERR_CANT_GRAN_PERMISSION_USB
                )
                else -> String.format(
                    Locale.FRANCE, "Unknown error %d, Internal Error = %d",
                    iCodeError, iInternalError
                )
            }
        }
    }
}