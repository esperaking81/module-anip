// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package co.opensi.fingerprintmanager.morpho_utils;

public enum FingerPrintMode
{
	Verify, Enroll
}
