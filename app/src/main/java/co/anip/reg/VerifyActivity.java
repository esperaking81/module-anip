package co.anip.reg;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import co.anip.module.FManager;
//import co.opensi.fingerprintmanager.ErrorCodes;
//import co.opensi.fingerprintmanager.ErrorManager;
//import co.opensi.fingerprintmanager.FingerPrintManager;
//import co.opensi.fingerprintmanager.FingerprintManagerCallback;
//import co.opensi.fingerprintmanager.morpho_utils.MorphoUtils;

public class VerifyActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        String sdcard = Environment.getExternalStorageDirectory().getPath();
        // FManager.setLogsPath(sdcard);

        String tplOne = sdcard + "/" + "tpl_1.iso-fmc-cs";
        String pathOne = sdcard + "/" + "tpl_1";
        String tplTwo = sdcard + "/" + "tpl_2.iso-fmc-cs";
        String tplThree = sdcard + "/" + "tpl_3.iso-fmc-cs";

        FManager fManager = FManager.getInstance(this);
//        FManager.setAllowDialog(true);
//
//        findViewById(R.id.buttonVerifyMatch).setOnClickListener(v -> {
//            try {
//                int result = fManager.verifyMatch(tplOne, tplTwo);
//
//                if (result == 1) {
//                    showMessage("Verification succeeded !");
//                } else if (result == 0) {
//                    showMessage("Verification failed !");
//                }
//            } catch (Exception e) {
//                showMessage("Une erreure s'est produite lors de la vérification");
//                e.printStackTrace();
//            }
//        });
//
//        findViewById(R.id.buttonVerify).setOnClickListener(v -> {
//            try {
//                fManager.verify(11, tplThree, res -> {
//                    if (res == 1) {
//                        // verify failed
//                    } else {
//                        // verify failed
//                    }
//                });
//            } catch (Exception e) {
//                showMessage("Erreur de vérification: " + e.getMessage());
//            }
//        });
//
        findViewById(R.id.buttonSave).setOnClickListener(v -> {
//            fManager.captureBoth(pathOne, 0, 8, this::cb);

//            fManager.captureImage(0, pathOne + "_image", this::cb);

            fManager.captureFinger(8, pathOne + "_finger", this::cb);
        });
    }

    private void cb(int res) {
        runOnUiThread(() -> {
            if (res == 1) {
                // verify success
                showMessage("Success !");
            } else {
                // verify failed
                showMessage("Failed !");
            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}